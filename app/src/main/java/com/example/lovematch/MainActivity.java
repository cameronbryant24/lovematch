package com.example.lovematch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn_compute;
    ImageView iv_needle;
    EditText et_yourname, et_otherpersonname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_compute = (Button) findViewById(R.id.btn_compute);
        iv_needle = (ImageView) findViewById(R.id.iv_needle);
        et_otherpersonname = (EditText) findViewById(R.id.et_otherpersonname);
        et_yourname = (EditText) findViewById(R.id.et_yourName);

        btn_compute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String yourname = et_yourname.getText().toString().toLowerCase();
                String otherpersonname = et_otherpersonname.getText().toString().toLowerCase();

                int totalLetters = yourname.length() + otherpersonname.length();
                int totalMatches = 0;

                for (int i = 0; i < yourname.length(); i++) {
                    for (int j = 0; j < otherpersonname.length(); j++) {
                        if (yourname.charAt(i) == otherpersonname.charAt(j)) {
                            totalMatches++;
                        }
                    }
                }
                for (int i = 0; i < otherpersonname.length(); i++) {
                    for (int j = 0; j < yourname.length(); j++) {
                        if (otherpersonname.charAt(i) == yourname.charAt(j)) {
                            totalMatches++;
                        }
                    }
                }

                        float compatScore = (float) totalMatches / totalLetters;
                        int loveScore = ((int) (compatScore * 100)) - 50;
                        RotateAnimation ra = new RotateAnimation(0, 360 + loveScore,
                                Animation.RELATIVE_TO_SELF, 0.5f,
                                Animation.RELATIVE_TO_SELF, 0.5f);

                        ra.setFillAfter(true);
                        ra.setDuration(2000);
                        ra.setInterpolator(new AccelerateDecelerateInterpolator());
                        iv_needle.startAnimation(ra);

                        Toast.makeText(MainActivity.this, "Love Score of " + loveScore, Toast.LENGTH_SHORT).show();
                    }
                })
                ;

            }}

